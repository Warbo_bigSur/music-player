const $ = document.querySelector.bind(document)
const $$ = document.querySelectorAll.bind(document)
const PLAYER_STORAGE_KEY = 'F8_PLAYER'
const heading = $('header h2')
const cdThumb = $('.cd-thumb')
const audio = $('#audio')
const playBtn = $('.btn-toggle-play')
const cd = $('.cd')
const player = $('.player')
const progress = $('#progress')
const prevBtn = $('.btn-prev')
const nextBtn = $('.btn-next')
const randomBtn = $('.btn-random')
const repeatBtn = $('.btn-repeat')
const playList = $('.playlist')
const skipTime = $('.skip-time')
const backTime = $('.re-time')



const app = {
    currentIndex: 0,
    isPlaying: false,
    isRandom: false,
    isRepeat: false,
    config: JSON.parse(localStorage.getItem(PLAYER_STORAGE_KEY)) || {},
    songs: [



        {
            name: 'Khi người đàn khóc ',
            singer: 'Lý Hải ',
            path: './assets/music/song9.mp3',
            image: './assets/img/Ly_hai_1.jpeg '

        },
        {
            name: 'Hãy trao cho anh',
            singer: 'Sơn Tùng MTP ',
            path: './assets/music/song2.mp3',
            image: './assets/img/MTP.jpeg'

        },
        {
            name: 'My heart will go on (remix)',
            singer: 'Thái Hoàng ',
            path: './assets/music/song3.mp3',
            image: './assets/img/IMG_0730.JPG'

        },
        {
            name: 'Tuý âm ',
            singer: 'Xesi ',
            path: './assets/music/song4.mp3',
            image: './assets/img/túy âm.jpeg'

        },
        {
            name: 'Yêu em là định mệnh ',
            singer: 'Cao Thái Sơn',
            path: './assets/music/song5.mp3',
            image: './assets/img/caothaison.jpeg'

        },
        {
            name: 'Anh Yêu Người Khác Rồi  ',
            singer: 'Khắc Việt',
            path: './assets/music/song6.mp3',
            image: './assets/img/images.jpeg'

        },
        {
            name: 'Cơn Đau Cuối Cùng Remix ',
            singer: ' Lê Hiếu ',
            path: './assets/music/song7-remix.mp3',
            image: './assets/img/le_hieu.jpeg'

        },
        {
            name: 'Không chỉ là thích ',
            singer: ' Phùng Đề Mạc',
            path: './assets/music/song8.mp3',
            image: './assets/img/phungdemac.jpeg'

        },
        {
            name: 'Where have you gone',
            singer: 'Surisan',
            path: './assets/music/song10.mp3',
            image: './assets/img/Surisan.jpeg'

        },
        {
            name: 'Giọt cà phê đầu tiên ',
            singer: 'Mạnh Quỳnh ft Trường Vũ',
            path: './assets/music/song11.mp3',
            image: './assets/img/trường vũ.jpeg'

        }


















    ],
    setConfig: function (key, value) {
        this.config[key] = value;
        localStorage.setItem(PLAYER_STORAGE_KEY, JSON.stringify(this.config))
    },
    render: function () {
        const htmls = this.songs.map((song, index) => {
            return ` <div class="song ${index === this.currentIndex ? 'active' : ''}" data-index="${index}">
<div class="thumb"
    style="background-image: url('${song.image}')">
</div>
<div class="body">
    <h3 class="title">${song.name}</h3>
    <p class="author">${song.singer}</p>
</div>
<div class="option">
    <i class="fas fa-ellipsis-h"></i>
</div>
</div>`

        })
        playList.innerHTML = htmls.join('')


    },
    defineProperties: function () {
        Object.defineProperty(this, "currentSong", {
            get: function () {
                return this.songs[this.currentIndex]
            }
        })
    },
    handleEvents: function () {
        const _this = this
        const cdWidth = cd.offsetWidth

        //  xử lí CD quay 
        const cdThumbAnimate = cdThumb.animate([
            { transform: 'rotate(360deg) ' }
        ], {
            duration: 10000,
            iterations: Infinity

        })
        cdThumbAnimate.pause()

        // xử lí khi phóng to / thu nhỏ CD
        document.onscroll = function () {
            const scrollTop = window.screenY || document.documentElement.scrollTop
            const newCdWidth = cdWidth - scrollTop
            cd.style.width = newCdWidth > 0 ? newCdWidth + 'px' : 0
            cd.style.opacity = newCdWidth / cdWidth
        }
        // xử lí khi click play
        playBtn.onclick = function () {
            if (_this.isPlaying) {

                audio.pause();

            } else {

                audio.play();

            }
        }
        // khi song đuợc play
        audio.onplay = function () {
            _this.isPlaying = true
            player.classList.add('playing')
            cdThumbAnimate.play()
        }
        // khi song bị pause
        audio.onpause = function () {
            _this.isPlaying = false
            player.classList.remove('playing')
            cdThumbAnimate.pause()
        }
        // khi tiến đô bài hát thay đổi
        audio.ontimeupdate = function () {
            if (audio.duration) {
                const progressPercent = Math.floor(audio.currentTime / audio.duration * 100)
                progress.value = progressPercent

            }
        }
        // xử lí khi skip time/return song
        backTime.onclick = function(){
audio.currentTime -=  10;

        }
        skipTime.onclick = function(){
audio.currentTime +=  10;
            
        }
        //  xử lí khi tua song 
        progress.onchange = function (e) {
            const seekTime = audio.duration / 100 * e.target.value
            audio.e = seekTime
        }
        // khi next song
        nextBtn.onclick = function () {
            if (_this.isRandom) {
                _this.playRandomSong()

            } else {
                _this.nextSong()

            }

            audio.play()
            _this.render()
            _this.scrollToActiveSong()
        }
        prevBtn.onclick = function () {
            if (_this.isRandom) {
                _this.playRandomSong()

            } else {
                _this.prevSong()

            }
            audio.play()
            _this.render()
            _this.scrollToActiveSong()


        }
        //  xử lí bật / tắt random
        randomBtn.onclick = function (e) {
            _this.isRandom = !_this.isRandom
            _this.setConfig('isRandom', _this.isRandom)
            randomBtn.classList.toggle('active', _this.isRandom)
        }
        // xử lí lặp lại 1 song                                 
        repeatBtn.onclick = function (e) {
            _this.isRepeat = !_this.isRepeat
            _this.setConfig('isRepeat', _this.isRepeat)

            repeatBtn.classList.toggle('active', _this.isRepeat)

        }

        // xử lí audio khi song ended
        audio.onended = function () {
            if (_this.isRepeat) {
                audio.play()
            } else {

                nextBtn.click()

            }
        }
        // lắng nghe hành vi click vào playLis
        playList.onclick = function (e) {
            const songNode = e.target.closest('.song:not(.active)')
            if (songNode || e.target.closest('.option')) {
                // xử lí khi click vào song
                if (songNode) {
                    _this.currentIndex = Number(songNode.dataset.index)
                    _this.loadCurrentSong()
                    audio.play()
                    _this.render()
                }

            }
        }


    },
    scrollToActiveSong: function () {
        setTimeout(() => {
            $('.song.active').scrollIntoView({
                behavior: 'smooth',
                block: 'center',

            })
        }, 300)

    },
    loadCurrentSong: function () {

        heading.textContent = this.currentSong.name
        cdThumb.style.backgroundImage = `url('${this.currentSong.image}' )`
        audio.src = this.currentSong.path
        // console.log(heading,cdThumb,audio)

    },
    loadConfig: function () {
        this.isRandom = this.config.isRandom
        this.isRepeat = this.config.isRepeat

    },

    nextSong: function () {
        this.currentIndex++
        if (this.currentIndex >= this.songs.length) {
            this.currentIndex = 0
        }
        this.loadCurrentSong()

    },
    prevSong: function () {
        this.currentIndex--
        if (this.currentIndex < 0) {
            this.currentIndex = this.songs.length - 1
        }

        this.loadCurrentSong()


    },    playRandomSong: function () {
        let newIndex;
        do {
            newIndex = Math.floor(Math.random() * this.songs.length)

        } while (newIndex === this.currentIndex)
        this.currentIndex = newIndex
        this.loadCurrentSong()

    },

    start: function () {
        // gán cấu hình từ config vào ứng dụng 
        this.loadConfig()
        //    Định nghĩa các thuộc tính cho object
        this.defineProperties()
        //  Lắng nghe/xử lí các sự kiện DOM(events)
        this.handleEvents()


        // tải thông tin bài hát đầu tiên
        this.loadCurrentSong()

        //    render playlist
        this.render()
        // hiển thị trạng thái ban đầu của btn repeat và random
        randomBtn.classList.toggle('active', this.isRandom)
        repeatBtn.classList.toggle('active', this.isRepeat)
    }

};
app.start()

